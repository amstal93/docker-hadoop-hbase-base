# tags (before "\t") and build arguments (after "\t", separated by " " which cannot be utilised otherwise) of the Docker image variants to build
# for versions, see the source Docker images of Hadoop and HBase
hadoop2.7-hbase2.2	HADOOP_VERSION=alpine-2.7.7 HBASE_VERSION=alpine-2.2.0
hadoop2.8-hbase2.2	HADOOP_VERSION=alpine-2.8.5 HBASE_VERSION=alpine-2.2.0
hadoop2.9-hbase2.2	HADOOP_VERSION=alpine-2.9.2 HBASE_VERSION=alpine-2.2.0
hadoop3.1-hbase2.2	HADOOP_VERSION=alpine-3.1.2 HBASE_VERSION=alpine-2.2.0
hadoop3.2-hbase2.2	HADOOP_VERSION=alpine-3.2.0 HBASE_VERSION=alpine-2.2.0
